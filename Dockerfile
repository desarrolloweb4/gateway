FROM openjdk:8-jdk-alpine
COPY "./target/gateway-0.0.1-SNAPSHOT.jar" "app.jar"
RUN apt-get update
RUN apt-get install nano net-tools
EXPOSE 2222
ENTRYPOINT [ "java", "-jar", "app.jar" ]