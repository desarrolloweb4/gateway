package com.api.gateway.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/fallback")
public class GatewayController {

	@GetMapping("/springboot")
	public Mono<String> spring(){
		return Mono.just("La api spring boot se tardo en responder");
	}

	@GetMapping("/nodejs")
	public Mono<String> nodejs(){
		return Mono.just("La api de nodejs se tardo en responder");
	}

	@GetMapping("/angular")
	public Mono<String> angular(){
		return Mono.just("Angular esta tardando en responder");
	}
	
}
